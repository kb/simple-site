# -*- encoding: utf-8 -*-
import logging

from django.conf import settings
from django.db import models, transaction
from django.db.models.fields import CharField, Field
from django.shortcuts import render
from wagtail.admin.panels import (
    FieldPanel,
    MultiFieldPanel,
    PageChooserPanel,
)
from wagtail.contrib.settings.models import BaseSiteSetting, register_setting
from wagtail.fields import StreamField
from wagtail.models import Page, Site

from enquiry.forms import EnquiryForm
from enquiry.models import Enquiry
from gdpr.models import Consent
from mail.tasks import process_mail
from .block import (
    DocumentBlock,
    GalleryBlock,
    HeroBlock,
    TeamBlock,
    TextImageBlock,
    TwoColumnTextBlock,
)


SECTION_BODY = "body"
SECTION_CARD = "card"
SECTION_GALLERY = "gallery"
SECTION_NEWS = "news"
SECTION_SLIDESHOW = "slideshow"


logger = logging.getLogger(__name__)


class ContactPage(Page):
    message = models.CharField(
        max_length=50,
        null=True,
        help_text="Message above contact form (`Your Contact Info Here` etc)",
    )
    card_title = models.CharField(
        max_length=50,
        null=True,
        help_text="Text above contact information",
    )
    card_description = models.CharField(
        max_length=300,
        null=True,
        help_text="Description of contact information (Why / When to contact etc)",
    )
    phone_number = models.CharField(
        max_length=50,
        null=True,
        help_text="Phone number (e.g +44 7........)",
    )
    email = models.CharField(
        max_length=50,
        null=True,
        help_text="Email address (e.g ryan....@email.com)",
    )
    address = models.TextField(
        max_length=255,
        null=True,
        help_text="Physical Address... (Leave blank if you don't have one)",
        blank=True,
    )
    map_link = models.URLField(
        help_text="URL to google maps (Leave blank if you dont have one)",
        null=True,
        blank=True,
    )
    map_caption = models.CharField(
        help_text="Caption for link, e.g 'Click for maps view'",
        null=True,
        max_length=30,
        blank=True,
    )
    thankyou_title = models.CharField(
        max_length=150,
        null=True,
        help_text="Description after contact form submission",
    )
    thankyou_message = models.CharField(
        max_length=50,
        null=True,
        help_text="Thank you message after form submission",
    )
    page_name = models.CharField(
        max_length=50, null=True, help_text="Page name on button"
    )
    page = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Go to this page after clicking the button",
    )

    content_panels = Page.content_panels + [
        FieldPanel("message"),
        FieldPanel("card_title"),
        FieldPanel("card_description"),
        FieldPanel("phone_number"),
        FieldPanel("email"),
        FieldPanel("address"),
        FieldPanel("map_link"),
        FieldPanel("map_caption"),
        FieldPanel("thankyou_title"),
        FieldPanel("thankyou_message"),
        FieldPanel("page_name"),
        PageChooserPanel("page"),
    ]

    def _consent(self):
        return Consent.objects.get_consent(Enquiry.GDPR_CONTACT_SLUG)

    def serve(self, request):
        context = super().get_context(request)
        kwargs = dict(consent=self._consent(), request=request)
        if request.method == "POST":
            form = EnquiryForm(request.POST, **kwargs)
            if form.is_valid():
                with transaction.atomic():
                    enquiry = form.save()
                    transaction.on_commit(lambda: process_mail.send())
                context.update({"enquiry": enquiry, "page": self})
                return render(request, self.template, context)
        else:
            form = EnquiryForm(**kwargs)
        context.update({"enquiry": None, "form": form, "page": self})
        return render(request, self.template, context)

    max_count = 1


class DefaultPage(Page):
    body = StreamField(
        [
            ("hero", HeroBlock()),
            ("text_image", TextImageBlock()),
            ("two_column_text", TwoColumnTextBlock()),
            ("Documents", DocumentBlock()),
            ("Gallery", GalleryBlock()),
            ("Team", TeamBlock()),
        ],
        use_json_field=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel("body"),
    ]


@register_setting
class FooterSettings(BaseSiteSetting):
    """Social media settings for our custom website."""

    facebook = models.URLField(
        blank=True,
        null=True,
        help_text="Facebook URL. Leave blank to remove it from the footer.",
    )
    instagram = models.URLField(
        blank=True,
        null=True,
        help_text="Instagram URL. Leave blank to remove it from the footer.",
    )
    linkedin = models.URLField(
        blank=True,
        null=True,
        help_text="Linkedin URL. Leave blank to remove it from the footer.",
    )
    twitter = models.URLField(
        blank=True,
        null=True,
        help_text="Twitter URL. Leave blank to remove it from the footer.",
    )
    company_name = models.CharField(
        blank=True,
        max_length=30,
        null=True,
        help_text="Your Company name Here!",
    )

    panels = [
        MultiFieldPanel(
            [
                FieldPanel("facebook"),
                FieldPanel("instagram"),
                FieldPanel("linkedin"),
                FieldPanel("twitter"),
                FieldPanel("company_name"),
            ]
        )
    ]


@register_setting
class SiteSettings(BaseSiteSetting):
    logo = models.ForeignKey(
        "wagtailimages.image",
        null=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Can be an SVG",
    )
    google_site_tag = models.CharField(
        blank=True,
        max_length=30,
        null=True,
        help_text="Your Site Google Analytic Tag!",
    )
    google_site_verification = models.CharField(
        blank=True,
        max_length=200,
        null=True,
        help_text="Your Site Google HTML Verification Content! e.g 'Jv3APl...'",
    )
    site_domain = models.CharField(
        blank=True,
        max_length=200,
        null=True,
        default=settings.DOMAIN,
        help_text="URL of site",
    )
    panels = [
        MultiFieldPanel(
            [
                FieldPanel("logo"),
                FieldPanel("google_site_tag"),
                FieldPanel("google_site_verification"),
                FieldPanel("site_domain"),
            ]
        )
    ]
