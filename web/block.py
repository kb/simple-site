# -*- encoding: utf-8 -*-
from wagtail import blocks
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.fields import StreamField
from wagtail.images.blocks import ImageChooserBlock


class HeroBlock(blocks.StructBlock):
    """Hero block."""

    title = blocks.CharBlock(required=True, help_text="Main hero title")
    subtitle = blocks.CharBlock(
        required=False,
        help_text="Small text below main title, (leave blank to hide)",
    )
    description = blocks.RichTextBlock(
        required=True, help_text="Description about the site / company"
    )
    image = ImageChooserBlock(required=True, help_text="Organisation image")
    button_1 = blocks.PageChooserBlock(
        required=False, help_text="Page Redirect (leave blank to hide)"
    )
    button_1_name = blocks.CharBlock(required=False, help_text="Text on button")
    button_2 = blocks.PageChooserBlock(
        required=False,
        help_text="Page 2 Redirect, Requires Link 1 (leave blank to hide)",
    )
    button_2_name = blocks.CharBlock(required=False, help_text="Text on button")

    class Meta:
        icon = "grip"
        template = "web/hero.html"


class TeamBlock(blocks.StructBlock):
    """Our team.

    Copied from *With large images*
    https://tailwindui.com/components/marketing/sections/team-sections

    """

    heading = blocks.CharBlock(required=True, help_text="Heading")
    sub_heading = blocks.CharBlock(required=False, help_text="Sub-heading")
    team_members = blocks.ListBlock(
        blocks.StructBlock(
            [
                ("picture", ImageChooserBlock(required=True)),
                ("name", blocks.CharBlock(required=True, max_length=40)),
                ("description", blocks.TextBlock(required=False)),
                ("page", blocks.PageChooserBlock(required=False)),
            ]
        )
    )

    class Meta:
        icon = "doc-full"
        template = "web/team.html"


class TextImageBlock(blocks.StructBlock):
    """Content block."""

    title = blocks.CharBlock(required=True, help_text="Main Image block Title")
    subtitle = blocks.CharBlock(
        required=False, help_text="Subtitle below title, (Leave blank to hide)"
    )
    introduction = blocks.RichTextBlock(
        required=True,
        help_text="Main Introduction (small bit of text introducing the post)",
    )
    content = blocks.RichTextBlock(
        required=True, help_text="Main description for the block"
    )
    image = ImageChooserBlock(required=True, help_text="Main image")
    image_credit = blocks.CharBlock(
        required=False,
        help_text="image Credit (Credit the imagegrapher, leave blank to hide)",
    )

    class Meta:
        icon = "image"
        template = "web/text_image.html"


class TwoColumnTextBlock(blocks.StructBlock):
    """Two column text block."""

    title = blocks.CharBlock(required=True, help_text="Main Text Title")
    subtitle = blocks.CharBlock(
        required=False, help_text="Small subtitle (Leave blank to hide)"
    )
    column_1 = blocks.RichTextBlock(
        required=True, help_text="Column 1 of Text (Left Side)"
    )
    column_2 = blocks.RichTextBlock(
        required=True, help_text="Column 2 of Text (Right Side)"
    )

    class Meta:
        icon = "doc-full"
        template = "web/two_column_text.html"


class GalleryBlock(blocks.StructBlock):
    title = blocks.CharBlock(
        required=True, max_length=40, help_text="Title of the Gallery section"
    )
    description = blocks.TextBlock(
        required=True,
        max_length=300,
        help_text="Small description of the gallery",
    )

    images = blocks.ListBlock(
        blocks.StructBlock(
            [
                (
                    "image",
                    ImageChooserBlock(required=True, help_text="Gallery Image"),
                ),
                (
                    "title",
                    blocks.CharBlock(
                        required=True,
                        help_text="Text under the image",
                        max_length=40,
                    ),
                ),
                (
                    "description",
                    blocks.TextBlock(
                        required=True,
                        help_text="Blue text underneath the title, small description.",
                        max_length=80,
                    ),
                ),
            ]
        )
    )

    class Meta:
        icon = "doc-full"
        template = "web/gallery.html"


class DocumentBlock(blocks.StructBlock):
    """Two column text block."""

    title = blocks.CharBlock(required=True)
    description = blocks.RichTextBlock(required=True)
    cards = blocks.ListBlock(
        blocks.StructBlock(
            [
                (
                    "date",
                    blocks.DateBlock(
                        required=True, help_text="Date the document was updated"
                    ),
                ),
                (
                    "title",
                    blocks.CharBlock(
                        required=True,
                        max_length=30,
                        help_text="Title of Document",
                    ),
                ),
                (
                    "subtitle",
                    blocks.CharBlock(
                        required=False,
                        max_length=30,
                        help_text="Piece of small text above the main title (if empty, will be blank)",
                    ),
                ),
                (
                    "image",
                    ImageChooserBlock(
                        required=True, help_text="Image above document"
                    ),
                ),
                (
                    "description",
                    blocks.TextBlock(
                        required=True,
                        max_length=400,
                        help_text="Document Description",
                    ),
                ),
                (
                    "document",
                    DocumentChooserBlock(
                        required=True, help_text="Downloadable Document"
                    ),
                ),
            ]
        )
    )

    class Meta:
        icon = "doc-full"
        template = "web/documents.html"
