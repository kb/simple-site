# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import SettingsView


urlpatterns = [
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
]
