# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings
from django.utils import timezone


logger = logging.getLogger(__name__)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME, max_retries=0)
def test_task_queue_default():
    logger.info(
        ">>> test_task_queue_default: {}".format(
            timezone.localtime(timezone.now()).strftime("%d/%m/%Y %H:%M")
        )
    )


def schedule_test_task_queue_default():
    logger.info(
        ">>> schedule_test_task_queue_default: {}".format(
            timezone.localtime(timezone.now()).strftime("%d/%m/%Y %H:%M")
        )
    )
    test_task_queue_default.send()
