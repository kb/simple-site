Simple Site (was ``hatherleighcommunitymarket_org``)
****************************************************

Development
===========

Install
-------

Virtual Environment
-------------------

If you've got dev-scripts installed type::

  create-venv simple-site

Otherwise::

  python3 -m venv venv-simple-site
  source venv-simple-site/bin/activate

  pip install --upgrade pip
  pip install -r requirements/local.txt

Testing
-------

::

  find . -name '*.pyc' -delete
  pytest -x

Create a development environment
--------------------------------

To configure a blank development environment use::

  ./init_dev.sh

Release and Deploy
==================

https://www.kbsoftware.co.uk/docs/


djhtml
------

::

  djhtml -t 2 -i template.html

tailwind
--------

From
https://tailwindcss.com/docs/installation#installing-tailwind-css-as-a-post-css-plugin

::

  cd front
  pnpm env use --global 20
  pnpm install
  # run the 'build:css' script from 'package.json'
  pnpm run build:css

  # increment version (``v1``) for cache busting
  <link rel="stylesheet" href="{% static 'web/styles.css' %}?v1" />
