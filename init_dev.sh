#!/bin/bash
# treat unset variables as an error when substituting.
set -u
# exit immediately if a command exits with a nonzero exit status.
set -e

DB_NAME="dev_simple_site_`id -nu`"
psql -X -U postgres -c "DROP DATABASE IF EXISTS ${DB_NAME};"
psql -X -U postgres -c "CREATE DATABASE ${DB_NAME} TEMPLATE=template0 ENCODING='utf-8';"

django-admin migrate --noinput
django-admin demo_data_login
django-admin init_project
django-admin init_app_enquiry
django-admin runserver 0.0.0.0:8000
