# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.sitemaps import GenericSitemap
from django.urls import path, re_path, include
from django.views.generic import RedirectView


from wagtail import urls as wagtail_urls
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.contrib.sitemaps.views import sitemap
from wagtail.documents import urls as wagtaildocs_urls


urlpatterns = [
    re_path(r"^dash/", view=include("dash.urls")),
    re_path(r"^dash/enquiry/", view=include("enquiry.urls")),
    re_path(r"^dash/gdpr/", view=include("gdpr.urls")),
    re_path(r"^dash/mail/", view=include("mail.urls")),
    re_path(
        r"^dash/not/used/$",
        view=RedirectView.as_view(
            pattern_name="project.settings", permanent=False
        ),
        name="project.dash",
    ),
    re_path(
        r"^dash/home/not/used/$",
        view=RedirectView.as_view(
            pattern_name="project.settings", permanent=False
        ),
        name="project.home",
    ),
    re_path(
        r"^dash/oidc/not/used/$",
        view=RedirectView.as_view(
            pattern_name="project.settings", permanent=False
        ),
        name="oidc_authentication_init",
    ),
    path("sitemap.xml/", sitemap),
    path("cms/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    re_path(r"", include(wagtail_urls)),
    # re_path(
    #     r"^sitemap\.xml$",
    #     view=sitemap,
    #     kwargs={"sitemaps": sitemaps},
    #     name="django.contrib.sitemaps.views.sitemap",
    # ),
    re_path(r"^", view=include("login.urls")),
    # re_path(r"^enquiry/", view=include("enquiry.urls")),
    # re_path(r"^gdpr/", view=include("gdpr.urls")),
    # re_path(r"^mail/", view=include("mail.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
