# -*- encoding: utf-8 -*-
import csv

from decimal import Decimal, InvalidOperation
from django.core.management.base import BaseCommand
from django.utils.http import urlencode
from django.utils import timezone


class Command(BaseCommand):
    """MailChimp - create a mailing list in CSV format #7225."""

    help = "MailChimp - create a mailing list in CSV format"

    def handle(self, *args, **options):
        """MailChimp - create a mailing list in CSV format.

        https://www.kbsoftware.co.uk/crm/ticket/7225/

        """
        self.stdout.write(f"{self.help}...")
        file_name = "2023-11-community-shares-pledges.tsv"
        date_as_str = timezone.now().strftime("%Y-%m-%d")
        file_name_output = f"{date_as_str}-mailchimp-mailing-list.csv"
        reader = csv.reader(open(file_name), "excel-tab")
        count = 0
        contact_another_way = []
        contact_version_2 = []
        total = Decimal()
        total_version_2 = Decimal()
        with open(file_name_output, "w", newline="", encoding="utf-8") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            csv_writer.writerow(["fullname", "phone", "email", "amount", "url"])
            for line in reader:
                count = count + 1
                if count < 3:
                    continue
                full_name = line[0]
                mobile = line[3]
                email = line[4]
                scheme = line[7]
                version_2 = False
                if "v2" in scheme:
                    version_2 = True
                try:
                    amount = Decimal(line[6])
                except InvalidOperation:
                    amount = Decimal()
                if version_2:
                    total_version_2 = total_version_2 + amount
                    contact_version_2.append(line)
                elif full_name and email and (amount > Decimal("99")):
                    total = total + amount
                    url = (
                        "https://www.fohm-cbs.org/update-pledge/?"
                        + urlencode(
                            {
                                "fullname": full_name,
                                "phone": mobile,
                                "email": email,
                                "amount": amount,
                            }
                        )
                    )
                    print(
                        f"{count:5} {email:40} {mobile:30} {full_name:40} {amount:10} {url}"
                    )
                    csv_writer.writerow([full_name, mobile, email, amount, url])
                else:
                    contact_another_way.append(line)
        print("")
        print("No name, email or pledge amount is wrong")
        file_name_output_no_email_etc = f"{date_as_str}-no-email-etc.csv"
        with open(
            file_name_output_no_email_etc, "w", newline="", encoding="utf-8"
        ) as out_no_email:
            csv_writer = csv.writer(out_no_email, dialect="excel-tab")
            for x in contact_another_way:
                csv_writer.writerow(x)
                print(x)
        print("")
        # print("Version 2")
        # for x in contact_version_2:
        #    print(x)
        self.stdout.write(
            (
                f"{self.help} - Complete - Total={total} "
                f"(version 2={total_version_2})"
            )
        )
