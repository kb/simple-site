Google My Business
******************

Open Fortnightly
================

My business is only open on fortnightly Saturdays. How do I set this?

  There is no provision on Google Business to set business hours that way.
  You will have to adjust the business hours every other week for Saturdays.

For details, see `Open Fortnightly`_


.. _`Open Fortnightly`: https://support.google.com/business/thread/169144599/open-fortnightly?hl=en
