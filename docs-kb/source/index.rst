.. Doc4Locks documentation master file, created by
   sphinx-quickstart on Fri Jan  3 08:57:02 2025.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Simple Site
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   google-my-business

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
